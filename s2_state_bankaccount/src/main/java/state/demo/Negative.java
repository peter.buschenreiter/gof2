package state.demo;

public class Negative implements AccountState {
    private final BankAccount account;
    private double balance;

    public Negative(BankAccount account) {
        balance = account.getBalance();
        this.account = account;
        initialise();
    }

    void initialise() {
        account.setInterest(0.0);
        account.setMinLimit(-500.0);
        account.setMaxLimit(1000.0);
    }



    public void deposit(double amount) {
        balance += amount;
        account.setBalance(balance);
        testStateChange();
    }

    public void withdraw(double amount) {
        System.out.println("Withdrawal refused!");
    }

    public void testStateChange(){
        if (balance > 0.0 &&
                balance < account.getMaxLimit()) {
            account.setState(new
              Normal(account));
        } else if (balance >
                account.getMaxLimit()) {
            account.setState(new
              WithInterest(account));
        }
    }


    public String toString() {
        return "Negative";
    }
}