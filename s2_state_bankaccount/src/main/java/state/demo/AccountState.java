package state.demo;

public interface AccountState {
    void deposit(double bedrag);

    void withdraw(double bedrag);

    default void payInterest() {
        // default no interest is payed
    };
}