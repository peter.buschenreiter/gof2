package state.demo;

public class BankAccount {
    private final String holder;
    private double balance;
    private double interest;
    private double minLimit;
    private double maxLimit;
    private AccountState state;

    public BankAccount(String holder) {
        this.holder = holder;
        state = new Normal(this);
    }

    public String getHolder() {
        return holder;
    }

    public double getBalance() {
        return balance;
    }

    public double getInterest() {
        return interest;
    }

    public double getMinLimit() {
        return minLimit;
    }

    public double getMaxLimit() {
        return maxLimit;
    }

    public void setState(AccountState status) {
        this.state = status;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public void setMinLimit(double minLimit) {
        this.minLimit = minLimit;
    }

    public void setMaxLimit(double maxLimit) {
        this.maxLimit = maxLimit;
    }

    public void deposit(double amount) {
        state.deposit(amount);
    }

    public void withdraw(double amount) {
        state.withdraw(amount);
    }

    public void payInterest() {
        state.payInterest();
    }

    public String toString() {
        return "Balance: " + balance + "  State: " + state;
    }
}