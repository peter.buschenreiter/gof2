package state.demo;

public class WithInterest implements AccountState {
    private final BankAccount account;
    private double balance;

    public WithInterest(BankAccount account) {
        balance = account.getBalance();
        this.account = account;
        initialiseer();
    }

    void initialiseer() {
        account.setInterest(0.005);
        account.setMinLimit(1000.0);
        account.setMaxLimit(1e10);
    }

    public void deposit(double bedrag) {
        balance += bedrag;
        account.setBalance(balance);
    }

    public void withdraw(double bedrag) {
        balance -= bedrag;
        account.setBalance(balance);
        testStateChange();
    }

    public void payInterest() {
        balance += account.getInterest() * balance;
        account.setBalance(balance);
    }

    private void testStateChange() {
        if (balance < 0.0) {
            account.setState(new Negative(account));
        } else if (balance < account.getMinLimit()) {
            account.setState(new Normal(account));
        }
    }

    public String toString() {
        return "With Interest";
    }
}