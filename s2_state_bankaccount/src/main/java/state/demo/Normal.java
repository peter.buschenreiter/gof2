package state.demo;

public class Normal implements AccountState {
    private final BankAccount account;
    private double balance;

    public Normal(BankAccount account) {
        balance = account.getBalance();
        this.account = account;
        initialise();
    }


    void initialise() {
        account.setInterest(0.0);
        account.setMinLimit(0.0);
        account.setMaxLimit(1000.0);
    }

    public void deposit(double amount) {
        balance += amount;
        account.setBalance(balance);
        testStateChange();
    }

    public void withdraw(double amount) {
        balance -= amount;
        account.setBalance(balance);
        testStateChange();
    }

    private void testStateChange() {
        if (balance < account.getMinLimit()) {
            account.setState(new Negative(account));
        } else if (balance > account.getMaxLimit()) {
            account.setState(new WithInterest(account));
        }
    }

    public String toString() {
        return "Normal";
    }
}