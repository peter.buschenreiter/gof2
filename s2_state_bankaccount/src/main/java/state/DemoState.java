package state;

import state.demo.BankAccount;

public class DemoState {
    public static void main(String[] args) {
        BankAccount account = new BankAccount("Jos The Boss");
        System.out.printf("Bankaccount of %s%n", account.getHolder());
        account.deposit(500);
        System.out.println(account);
        account.deposit(850);
        System.out.println(account);
        account.payInterest();
        System.out.println(account);
        account.withdraw(1100);
        System.out.println(account);
        account.withdraw(500);
        System.out.println(account);
        account.withdraw(500);
        System.out.println(account);
    }
}

/*
Bankaccount of Jos The Boss
Balance: 500.0  State: Normal
Balance: 1350.0  State: With Interest
Balance: 1356.75  State: With Interest
Balance: 256.75  State: Normal
Balance: -243.25  State: Negative
Withdrawal refused!
Balance: -243.25  State: Negative
*/