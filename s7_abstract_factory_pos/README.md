This is a version of the POS case (larman) with
- a Discount strategy
- JUnit 5
- A stateless controller
- an abstract repository factory with these implementations
  - a memory repository
  - a db repository (non functional)