package singleton;

/*
  Singleton op de klassieke wijze (voorkeur)
 */
public final class President {
    private static final President instance = new President("unknown","unknown");
    private String name;
    private String party;

    // private constructor

    private President(String name, String party) {
        this.name = name;
        this.party = party;
    }

    public static  President getInstance() {
        return instance;
    }

    public String getName() {
        return name;
    }

    public String getParty() {
        return party;
    }

    public void setPresident(String name, String party) {
        this.name = name;
        this.party = party;
    }

    @Override
    public String toString() {
        return "President: " + name + ", party: " + party;
    }

}
