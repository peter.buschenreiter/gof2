package singleton;

/*
  Singleton via enum
 */
public enum President {
    INSTANCE;

    private String name ="unknown";
    private String party ="unknown";

    // constructor of enum is implicitly private,

    // getters
    public String getName() {
        return name;
    }

    public String getParty() {
        return party;
    }

    // setter
    public void setPresident(String name, String party) {
        this.name = name;
        this.party = party;
    }

    @Override
    public String toString() {
        return "singleton.President: " + name + ", party: " + party;
    }
}
