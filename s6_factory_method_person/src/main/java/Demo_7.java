import staticfactory.Person;
import staticfactory.PersonFactory;
import staticfactory.NationalRegistryNumberFactory;

public class Demo_7 {
    public static void main(String[] args) {
        Person p = PersonFactory.newInstance("Sylvia Muyshondt", "63.12.05 254-38");
        System.out.println(p);

        Person p2 = PersonFactory.newInstance(
                "Sylvia Muyshondt",
                NationalRegistryNumberFactory.newNumber("63.12.05 254-38"));
        System.out.println(p2);

        Person p3 = PersonFactory.newInstance("Sylvia Muyshondt");
        System.out.println(p3);

        System.out.println("name = " + p.getName());
        System.out.println("number = " + p.getNumber());

        System.out.println("as long = " + p.getNumber().toLong());

    }
}

