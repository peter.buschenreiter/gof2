package staticfactory;

public class Citizen implements Person {
    private String name;
    private NationalRegisterNumber number;

    public Citizen(String name, NationalRegisterNumber number) {
        this.name = name;
        this.number = number;
    }

    public Citizen(String name, String number) {
        this(name, NationalRegistryNumberFactory.newNumber(number));
    }

    public String getName() {
        return name;
    }

    public NationalRegisterNumber getNumber() {
        return number;
    }

    public String toString() {
        return name + " " + number;
    }
}
